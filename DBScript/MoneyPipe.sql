create database if not exists MoneyPipe;
use MoneyPipe;
create table if not exists pipeline (
	id bigint auto_increment primary key,
    name varchar(100) not null,
    description varchar(200) not null,
    start_time timestamp(3),
    end_time timestamp(3),
    execution_status varchar(30) not null
);
create table if not exists pipe_task(
	id bigint auto_increment primary key,
    pipline_id bigint,
    name varchar(100) not null,
    description varchar(200) not null,
    start_time timestamp(3),
    end_time timestamp(3),
	pipe_task_action varchar(30) not null,
    execution_status varchar(30) not null,
    constraint pipline foreign key(pipline_id) references pipeline(id)
);
