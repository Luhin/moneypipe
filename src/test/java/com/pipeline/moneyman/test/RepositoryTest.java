package com.pipeline.moneyman.test;

/**
 * Created by Владимир on 08.02.2017.
 */

import com.pipeline.moneyman.configuration.AppConfigutation;
import com.pipeline.moneyman.entity.Pipeline;
import com.pipeline.moneyman.service.impl.PipelineService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration()
@ContextConfiguration(classes = AppConfigutation.class)
public class RepositoryTest {
    @Inject
    private PipelineService pipelineService;

    @Test
    public void fff(){
        Pipeline pipeline = new Pipeline();
        pipeline.setPipelineName("TTTTTTTTTTTTTTT");
        pipeline.setPipelineDescription("DdeeeeedTTTTTTTT");
        pipeline.setPipelineStartTime(LocalDateTime.now());
        pipeline.setPipelineEndTime(LocalDateTime.now().plusDays(2));
        pipeline.setPipelineTasks(new ArrayList<>());
        pipelineService.createPipeline(pipeline);
        Pipeline second = pipelineService.findPipeline(pipeline.getPipelineId());
        Assert.assertFalse(pipeline == second);

    }

    /*@Inject
    private PipelineRepository pipelineRepository;
    /*@Inject
    private PipeTaskRepository pipeTaskRepository;*/

    /*@Test
    public void mainTest() throws IOException {
        Pipeline pipeline = pipelineService.parseYamlFromFile();
        pipelineService.createPipeline(pipeline);
        pipelineService.executePipeline(pipeline.getPipelineId());

    }*/

    /*@Test
    public void yamlTest(){
        Pipeline pipeline = pipelineService.parseYamlFromFile();
        System.out.println(pipeline.getPipelineTasks().get(0).getTaskName());
    }*/

    /*@Test
    public void temp() {
        Pipeline pipeline = new Pipeline();
        pipeline.setPipelineName("nameeee");
        pipeline.setPipelineDescription("Ddeeeeede");
        pipeline.setPipelineStartTime(LocalDateTime.now());
        pipeline.setPipelineEndTime(LocalDateTime.now().plusDays(2));
        pipelineService.createPipeline(pipeline);
    }*/

    /*@Test
    public void findStatus(){
        String ps = pipelineService.showPipelineStatus(0L);
        System.out.println(ps);
    }*/

}
