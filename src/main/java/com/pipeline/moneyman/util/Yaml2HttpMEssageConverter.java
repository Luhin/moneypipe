package com.pipeline.moneyman.util;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;

/**
 * Created by Владимир on 10.02.2017.
 */
public final class Yaml2HttpMEssageConverter extends AbstractJackson2HttpMessageConverter {
    public Yaml2HttpMEssageConverter() {
        super(new YAMLMapper(), MediaType.parseMediaType("application/x-yaml"));
    }
}
