package com.pipeline.moneyman.controller.exceptionhandler;

/**
 * Created by Владимир on 10.02.2017.
 */
public class ErrorDetails {
    private String errorMessage;

    public ErrorDetails(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
