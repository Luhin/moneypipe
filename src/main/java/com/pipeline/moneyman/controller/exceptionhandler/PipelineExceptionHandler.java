package com.pipeline.moneyman.controller.exceptionhandler;

import com.pipeline.moneyman.exception.NotActivePipelineException;
import com.pipeline.moneyman.exception.PipelineBusyException;
import com.pipeline.moneyman.exception.PipelineNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

/**
 * Created by Владимир on 10.02.2017.
 */
@ControllerAdvice
public class PipelineExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> onException(Exception exception) {
        ErrorDetails errorDetails = new ErrorDetails(exception.getMessage());
        return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(PipelineNotFoundException.class)
    public ResponseEntity<ErrorDetails> onPipelineNotFoundException(PipelineNotFoundException exception) {
        ErrorDetails errorDetails = new ErrorDetails(exception.getMessage());
        return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PipelineBusyException.class)
    public ResponseEntity<ErrorDetails> onPipelineBusyExeption(PipelineBusyException exception) {
        ErrorDetails errorDetails = new ErrorDetails(exception.getMessage());
        return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.LOCKED);

    }

    @ExceptionHandler(NotActivePipelineException.class)
    public ResponseEntity<ErrorDetails> onNotActivePipelineException(NotActivePipelineException exception) {
        ErrorDetails errorDetails = new ErrorDetails(exception.getMessage());
        return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.CONFLICT);

    }
}
