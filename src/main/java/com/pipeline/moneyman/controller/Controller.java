package com.pipeline.moneyman.controller;

import com.pipeline.moneyman.entity.ExecutionStatus;
import com.pipeline.moneyman.entity.Pipeline;
import com.pipeline.moneyman.service.IPipelineService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
public class Controller {
    @Inject
    private IPipelineService pipelineService;

    @RequestMapping(value = "/pipeline/", method = RequestMethod.POST
            , consumes = "application/x-yaml", produces = "application/x-yaml")
    public ResponseEntity<Pipeline> createPipeline(@RequestBody Pipeline pipeline) {
        Pipeline createdPipeline = pipelineService.createPipeline(pipeline);
        return new ResponseEntity<Pipeline>(createdPipeline, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/pipeline/{pipelineId}/execute/", method = RequestMethod.POST
            , produces = "application/x-yaml")
    public ResponseEntity<Pipeline> executePipeline(@PathVariable Long pipelineId) {
        Pipeline pipeline = pipelineService.executePipeline(pipelineId);
        return new ResponseEntity<Pipeline>(pipeline, HttpStatus.OK);
    }

    @RequestMapping(value = "/pipeline/{pipelineId}", method = RequestMethod.GET
            , produces = "application/x-yaml")
    public ResponseEntity<Pipeline> showPipelineStatus(@PathVariable Long pipelineId) {
        Pipeline pipeline = pipelineService.findPipeline(pipelineId);
        return new ResponseEntity<Pipeline>(pipeline, HttpStatus.OK);
    }

    @RequestMapping(value = "/pipeline/{pipelineId}/stop_execution/", method = RequestMethod.PUT
            , produces = "application/x-yaml")
    public ResponseEntity<Pipeline> stopPipelineExecution(@PathVariable Long pipelineId) {
        Pipeline pipeline = pipelineService.stopPipelineExecution(pipelineId);
        return new ResponseEntity<Pipeline>(pipeline, HttpStatus.OK);
    }

    @RequestMapping(value = "/pipeline/{pipelineId}", method = RequestMethod.PUT
            , consumes = "application/x-yaml", produces = "application/x-yaml")
    public ResponseEntity<Pipeline> updatePipeline(@PathVariable Long pipelineId
            , @RequestBody Pipeline pipeline) {
        Pipeline updatedPipeline = pipelineService.updatePipeline(pipelineId, pipeline);
        return new ResponseEntity<Pipeline>(updatedPipeline, HttpStatus.OK);

    }

    @RequestMapping(value = "/pipeline/{pipelineId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePipeline(@PathVariable Long pipelineId) {
        pipelineService.deletePipeline(pipelineId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}