package com.pipeline.moneyman.exception;

/**
 * Created by Владимир on 10.02.2017.
 */
public class PipelineNotFoundException extends RuntimeException {
    public PipelineNotFoundException() {
        super("Pipeline not found");
    }
}
