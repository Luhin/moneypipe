package com.pipeline.moneyman.exception;

/**
 * Created by Владимир on 08.02.2017.
 */
public class FailedTaskException extends RuntimeException {
    public FailedTaskException() {
        super("Pipetask execution failed");
    }
}
