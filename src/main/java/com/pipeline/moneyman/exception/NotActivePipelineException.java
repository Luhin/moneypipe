package com.pipeline.moneyman.exception;

/**
 * Created by Владимир on 10.02.2017.
 */
public class NotActivePipelineException extends RuntimeException {
    public NotActivePipelineException() {
        super("Pipeline not active");
    }
}
