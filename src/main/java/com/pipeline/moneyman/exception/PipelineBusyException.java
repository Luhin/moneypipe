package com.pipeline.moneyman.exception;

/**
 * Created by Владимир on 10.02.2017.
 */
public class PipelineBusyException extends RuntimeException {
    public PipelineBusyException() {
        super("Pipeline is in progress");
    }
}
