package com.pipeline.moneyman.exception;

/**
 * Created by Владимир on 09.02.2017.
 */
public class FailedPipeLineException extends RuntimeException {
    public FailedPipeLineException() {
        super("Pipeline execution failed");
    }
}
