package com.pipeline.moneyman.constant;

/**
 * Created by Владимир on 08.02.2017.
 */
public class ExecutionMessage {
    public static final String PIPELINE = "Pipeline \"";
    public static final String PIPLINE_EXECUTION_START = "\" start execution";
    public static final String PIPLINE_EXECUTION_END = "\" end with status: ";
    public static final String PIPE_TASK = "Task \"";
    public static final String PIPE_TASK_EXECUTION_START = "\" start execution";
    public static final String PIPE_TASK_EXECUTION_END = "\" end with status: ";

    private ExecutionMessage(){

    }
}
