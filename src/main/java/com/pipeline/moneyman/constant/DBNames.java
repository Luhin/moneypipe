package com.pipeline.moneyman.constant;

/**
 * Created by Владимир on 09.02.2017.
 */
public class DBNames {
    public static final String PIPELINE_TABLE = "pipeline";
    public static final String PIPELINE_ID = "id";
    public static final String PIPELINE_NAME = "name";
    public static final String PIPELINE_DESCRIPTION = "description";
    public static final String PIPELINE_START_TIME = "start_time";
    public static final String PIPELINE_END_TIME = "end_time";
    public static final String PIPELINE_EXECUTION_STATUS = "execution_status";
    public static final String PIPE_TASK_TABLE = "pipe_task";
    public static final String PIPE_TASK_ID = "id";
    public static final String PIPE_LINE_ID_FOREIGN = "pipline_id";
    public static final String PIPE_TASK_NAME = "name";
    public static final String PIPE_TASK_DESCRIPTION = "description";
    public static final String PIPE_TASK_START_TIME = "start_time";
    public static final String PIPE_TASK_END_TIME = "end_time";
    public static final String PIPE_TASK_ACTION = "pipe_task_action";
    public static final String PIPE_TASK_EXECUTION_STATUS = "execution_status";

    private DBNames(){

    }
}
