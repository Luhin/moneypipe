package com.pipeline.moneyman.service.impl;

import com.pipeline.moneyman.constant.ExecutionMessage;
import com.pipeline.moneyman.entity.ExecutionStatus;
import com.pipeline.moneyman.entity.PipeTask;
import com.pipeline.moneyman.entity.PipeTaskAction;
import com.pipeline.moneyman.entity.Pipeline;
import com.pipeline.moneyman.exception.FailedPipeLineException;
import com.pipeline.moneyman.exception.FailedTaskException;
import com.pipeline.moneyman.repository.PipeTaskRepository;
import com.pipeline.moneyman.service.IPipeTaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * Created by Владимир on 08.02.2017.
 */
@Service
public class PipeTaskService implements IPipeTaskService {
    private final static Logger LOGGER = LogManager.getLogger();

    @Inject
    private PipeTaskRepository pipeTaskRepository;

    @Override
    public void startPipeTasks(List<PipeTask> pipeTasks) throws FailedPipeLineException {
        try {
            for (PipeTask pipeTask : pipeTasks) {
                startPipeTask(pipeTask);
            }
        } catch (FailedTaskException e) {
            skipOther(pipeTasks);
            throw new FailedPipeLineException();
        }
    }

    private void startPipeTask(PipeTask pipeTask) {
        try {
            if (canTaskStart(pipeTask)) {
                executePipeTask(pipeTask);
            }
        } catch (InterruptedException e) {
            throw new FailedTaskException();
        }
    }

    private boolean canTaskStart(PipeTask pipeTask) throws InterruptedException {
        Lock pipelineLock = pipeTask.getPipeline().getPipelinelock();
        pipelineLock.tryLock(200L, TimeUnit.MILLISECONDS);
        try {
            return pipeTask.getTaskExecutionStatus() == ExecutionStatus.PENDING;
        } finally {
            pipelineLock.unlock();
        }
    }

    private void executePipeTask(PipeTask pipeTask) throws FailedTaskException {
        initializePipeTask(pipeTask);
        pipeTaskRepository.saveAndFlush(pipeTask);
        pipeTask.getPipeTaskAction().action(pipeTask);
        finalizePipeTask(pipeTask);
        pipeTaskRepository.saveAndFlush(pipeTask);
    }

/*    private void checkRandomResult(PipeTask pipeTask) throws FailedTaskException {
        if (pipeTask.getTaskExecutionStatus() == ExecutionStatus.FAILED) {
            finalizePipeTask(pipeTask);
            pipeTaskRepository.saveAndFlush(pipeTask);
            throw new FailedTaskException();
        } else if (pipeTask.getTaskExecutionStatus() == ExecutionStatus.PENDING
                || pipeTask.getTaskExecutionStatus() == ExecutionStatus.IN_PROGRESS) {
            randomAction(pipeTask);
        }
    }*/

    @Override
    public void stopAllTasks(Pipeline pipeline) {
        try {
            Lock pipelineLock = pipeline.getPipelinelock();
            pipelineLock.tryLock(200L, TimeUnit.MILLISECONDS);
            try {
                skipOther(pipeline.getPipelineTasks());
            } finally {
                pipelineLock.unlock();
            }
        } catch (InterruptedException e) {
            throw new FailedTaskException();
        }
    }

    private void skipOther(List<PipeTask> pipeTasks) {
        pipeTasks.forEach(pipeTask -> {
            if (pipeTask.getTaskExecutionStatus() == ExecutionStatus.PENDING) {
                pipeTask.setTaskExecutionStatus(ExecutionStatus.SKIPPED);
            }
        });

    }

    private void initializePipeTask(PipeTask pipeTask) {
        pipeTask.setTaskExecutionStatus(ExecutionStatus.IN_PROGRESS);
        LocalDateTime startTime = LocalDateTime.now();
        pipeTask.setTaskStartTime(startTime);
        LOGGER.info(ExecutionMessage.PIPE_TASK + pipeTask.getTaskName()
                + ExecutionMessage.PIPE_TASK_EXECUTION_START);
    }

    private void finalizePipeTask(PipeTask pipeTask) {
        LocalDateTime endTime = LocalDateTime.now();
        pipeTask.setTaskEndTime(endTime);
        LOGGER.info(ExecutionMessage.PIPE_TASK + pipeTask.getTaskName()
                + ExecutionMessage.PIPE_TASK_EXECUTION_END + pipeTask.getTaskExecutionStatus());
    }
}
