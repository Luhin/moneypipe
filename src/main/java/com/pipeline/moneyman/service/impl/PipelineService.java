package com.pipeline.moneyman.service.impl;

import com.pipeline.moneyman.constant.ExecutionMessage;
import com.pipeline.moneyman.entity.ActivePipelineHolder;
import com.pipeline.moneyman.entity.ExecutionStatus;
import com.pipeline.moneyman.entity.PipeTask;
import com.pipeline.moneyman.entity.Pipeline;
import com.pipeline.moneyman.exception.FailedPipeLineException;
import com.pipeline.moneyman.exception.NotActivePipelineException;
import com.pipeline.moneyman.exception.PipelineBusyException;
import com.pipeline.moneyman.exception.PipelineNotFoundException;
import com.pipeline.moneyman.repository.PipelineRepository;
import com.pipeline.moneyman.service.IPipeTaskService;
import com.pipeline.moneyman.service.IPipelineService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.*;
import java.util.List;
import java.util.concurrent.ExecutorService;


/**
 * Created by Владимир on 08.02.2017.
 */
@Service
public class PipelineService implements IPipelineService {
    private final static Logger LOGGER = LogManager.getLogger();
    @Inject
    private IPipeTaskService pipeTaskService;
    @Inject
    private PipelineRepository pipelineRepository;
    @Inject
    private ExecutorService executorService;
    @Inject
    private ActivePipelineHolder activePipelineHolder;

    @Override
    public Pipeline createPipeline(Pipeline pipeline) {
        if (!pipeline.getPipelineTasks().isEmpty()) {
            assignPipeTaskToPipeline(pipeline);
        }
        return pipelineRepository.saveAndFlush(pipeline);
    }

    @Override
    public Pipeline executePipeline(Long pipelineId) {
        Pipeline pipeline = findPipeline(pipelineId);
        initializePipeline(pipeline);
        pipelineRepository.saveAndFlush(pipeline);
        executorService.submit(() -> {
            runPipeline(pipeline);
        });
        return pipeline;
    }

    @Override
    public void runPipeline(Pipeline pipeline) {
        executePipeTasks(pipeline);
        finalizePipeline(pipeline);
        pipelineRepository.saveAndFlush(pipeline);
    }

    @Override
    public Pipeline updatePipeline(Long pipelineId, Pipeline pipeline) {
        Pipeline persistPipeline = findPipeline(pipelineId);
        if (persistPipeline.getPipelineExecutionStatus() == ExecutionStatus.IN_PROGRESS) {
            throw new PipelineBusyException();
        }
        pipeline.setPipelineId(pipelineId);
        if (!pipeline.getPipelineTasks().isEmpty()) {
            assignPipeTaskToPipeline(pipeline);
        }
        return pipelineRepository.saveAndFlush(pipeline);
    }

    @Override
    public Pipeline findPipeline(Long pipelineId) {
        Pipeline pipeline = pipelineRepository.findOne(pipelineId);
        if (pipeline == null) {
            throw new PipelineNotFoundException();
        }
        return pipeline;
    }

    @Override
    public Pipeline stopPipelineExecution(Long pipelineId) {
        Pipeline pipeline = activePipelineHolder.getActivePipeline(pipelineId);
        if (pipeline == null) {
            throw new NotActivePipelineException();
        }
        pipeTaskService.stopAllTasks(pipeline);
        return pipeline;
    }

    @Override
    public void deletePipeline(Long pipelineId) {
        try {
            pipelineRepository.delete(pipelineId);
        } catch (EmptyResultDataAccessException e) {
            throw new PipelineNotFoundException();
        }
    }

    private void initializePipeline(Pipeline pipeline) {
        LocalDateTime startTime = LocalDateTime.now();
        pipeline.setPipelineStartTime(startTime);
        LOGGER.info(ExecutionMessage.PIPELINE + pipeline.getPipelineName()
                + ExecutionMessage.PIPLINE_EXECUTION_START);
        pipeline.setPipelineExecutionStatus(ExecutionStatus.IN_PROGRESS);
        pipeline.getPipelineTasks().forEach(pipeTask -> {
            pipeTask.setTaskExecutionStatus(ExecutionStatus.PENDING);
        });
    }

    private void executePipeTasks(Pipeline pipeline) {

        try {
            List<PipeTask> pipeTasks = pipeline.getPipelineTasks();
            activePipelineHolder.addPipeline(pipeline);
            pipeTaskService.startPipeTasks(pipeTasks);
            pipeline.setPipelineExecutionStatus(ExecutionStatus.COMPLETED);
        } catch (FailedPipeLineException e) {
            pipeline.setPipelineExecutionStatus(ExecutionStatus.FAILED);
        } finally {
            Long pipelineId = pipeline.getPipelineId();
            activePipelineHolder.removePipeline(pipelineId);
        }
    }

    private void finalizePipeline(Pipeline pipeline) {
        LocalDateTime endTime = LocalDateTime.now();
        pipeline.setPipelineEndTime(endTime);
        LOGGER.info(ExecutionMessage.PIPELINE + pipeline.getPipelineName()
                + ExecutionMessage.PIPLINE_EXECUTION_END + pipeline.getPipelineExecutionStatus());
    }

    private void assignPipeTaskToPipeline(Pipeline pipeline) {
        pipeline.getPipelineTasks().forEach(pipeTask -> {
            pipeTask.setPipeline(pipeline);
        });
    }
}
