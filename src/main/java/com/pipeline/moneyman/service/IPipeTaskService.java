package com.pipeline.moneyman.service;

import com.pipeline.moneyman.entity.PipeTask;
import com.pipeline.moneyman.entity.Pipeline;
import com.pipeline.moneyman.exception.FailedPipeLineException;

import java.util.List;

/**
 * Created by Владимир on 10.02.2017.
 */
public interface IPipeTaskService {
    void startPipeTasks(List<PipeTask> pipeTasks) throws FailedPipeLineException;

    void stopAllTasks(Pipeline pipeline);

}
