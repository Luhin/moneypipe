package com.pipeline.moneyman.service;

import com.pipeline.moneyman.entity.Pipeline;

/**
 * Created by Владимир on 10.02.2017.
 */
public interface IPipelineService {
    Pipeline createPipeline(Pipeline pipeline);

    Pipeline executePipeline(Long pipelineId);

    void runPipeline(Pipeline pipeline);

    Pipeline findPipeline(Long pipelineId);

    Pipeline updatePipeline(Long pipelineId, Pipeline pipeline);

    Pipeline stopPipelineExecution(Long pipelineId);

    void deletePipeline(Long pipelineId);
}
