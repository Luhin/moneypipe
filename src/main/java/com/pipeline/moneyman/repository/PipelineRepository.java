package com.pipeline.moneyman.repository;

import com.pipeline.moneyman.entity.Pipeline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Владимир on 09.02.2017.
 */
public interface PipelineRepository extends JpaRepository<Pipeline, Long> {

    @Query("select p.pipelineExecutionStatus from Pipeline p where p.pipelineId = :pipelineId")
    String getPipelineStatus(@Param("pipelineId") Long pipelineId);
}
