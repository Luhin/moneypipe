package com.pipeline.moneyman.repository;

import com.pipeline.moneyman.entity.PipeTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Владимир on 09.02.2017.
 */
public interface PipeTaskRepository extends JpaRepository<PipeTask, Long> {
}
