package com.pipeline.moneyman.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pipeline.moneyman.constant.DBNames;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Владимир on 08.02.2017.
 */
@Component
@Scope("prototype")
@Entity
@Table(name = DBNames.PIPELINE_TABLE)
public class Pipeline {
    @Id
    @Column(name = DBNames.PIPELINE_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pipelineId;
    @Column(name = DBNames.PIPELINE_NAME)
    private String pipelineName;
    @Column(name = DBNames.PIPELINE_DESCRIPTION)
    private String pipelineDescription;
    @Column(name = DBNames.PIPELINE_START_TIME)
    private LocalDateTime pipelineStartTime;
    @Column(name = DBNames.PIPELINE_END_TIME)
    private LocalDateTime pipelineEndTime;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = DBNames.PIPELINE_TABLE, cascade = CascadeType.ALL)
    private List<PipeTask> pipelineTasks;
    @Column(name = DBNames.PIPELINE_EXECUTION_STATUS)
    @Enumerated(EnumType.STRING)
    private ExecutionStatus pipelineExecutionStatus;
    @Transient
    @JsonIgnore
    private Lock pipelinelock = new ReentrantLock();

    public Lock getPipelinelock() {
        return pipelinelock;
    }

    public Long getPipelineId() {
        return pipelineId;
    }

    public void setPipelineId(Long pipelineId) {
        this.pipelineId = pipelineId;
    }

    public String getPipelineName() {
        return pipelineName;
    }

    public void setPipelineName(String pipelineName) {
        this.pipelineName = pipelineName;
    }

    public String getPipelineDescription() {
        return pipelineDescription;
    }

    public void setPipelineDescription(String pipelineDescription) {
        this.pipelineDescription = pipelineDescription;
    }

    public List<PipeTask> getPipelineTasks() {
        return pipelineTasks;
    }

    public void setPipelineTasks(ArrayList<PipeTask> pipelineTasks) {
        this.pipelineTasks = pipelineTasks;
    }

    public ExecutionStatus getPipelineExecutionStatus() {
        return pipelineExecutionStatus;
    }

    public void setPipelineExecutionStatus(ExecutionStatus pipelineExecutionStatus) {
        this.pipelineExecutionStatus = pipelineExecutionStatus;
    }

    public LocalDateTime getPipelineStartTime() {
        return pipelineStartTime;
    }

    public void setPipelineStartTime(LocalDateTime pipelineStartTime) {
        this.pipelineStartTime = pipelineStartTime;
    }

    public LocalDateTime getPipelineEndTime() {
        return pipelineEndTime;
    }

    public void setPipelineEndTime(LocalDateTime pipelineEndTime) {
        this.pipelineEndTime = pipelineEndTime;
    }
}
