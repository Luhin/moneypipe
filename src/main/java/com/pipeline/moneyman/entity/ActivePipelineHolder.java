package com.pipeline.moneyman.entity;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class ActivePipelineHolder {
    private ConcurrentHashMap<Long, Pipeline> activePipeliines = new ConcurrentHashMap<>();

    public void addPipeline(Pipeline pipeline) {
        Long pipelineId = pipeline.getPipelineId();
        activePipeliines.putIfAbsent(pipelineId, pipeline);
    }

    public Pipeline getActivePipeline(Long pipelineId) {
        return activePipeliines.get(pipelineId);
    }

    public void removePipeline(Long pipelineId) {
        activePipeliines.remove(pipelineId);
    }
}
