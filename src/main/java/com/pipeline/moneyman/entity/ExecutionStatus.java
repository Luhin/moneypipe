package com.pipeline.moneyman.entity;



import java.util.Random;

public enum ExecutionStatus {
    PENDING, IN_PROGRESS, SKIPPED, FAILED, COMPLETED;

    public static ExecutionStatus getRandomExecutionStatus() {
        return values()[new Random().nextInt(values().length)];
    }
}
