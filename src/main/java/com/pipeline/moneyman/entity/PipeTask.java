package com.pipeline.moneyman.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pipeline.moneyman.constant.DBNames;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Владимир on 08.02.2017.
 */
@Component
@Scope("prototype")
@Entity
@Table(name = DBNames.PIPE_TASK_TABLE)
public class PipeTask {
    @Id
    @Column(name = DBNames.PIPE_TASK_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long taskId;
    @ManyToOne
    @JoinColumn(name = DBNames.PIPE_LINE_ID_FOREIGN)
    @JsonIgnore
    private Pipeline pipeline;
    @Column(name = DBNames.PIPE_TASK_NAME)
    private String taskName;
    @Column(name = DBNames.PIPE_TASK_DESCRIPTION)
    private String taskDescription;
    @Column(name = DBNames.PIPE_TASK_START_TIME)
    private LocalDateTime taskStartTime;
    @Column(name = DBNames.PIPE_TASK_END_TIME)
    private LocalDateTime taskEndTime;
    @Column(name = DBNames.PIPE_TASK_ACTION)
    @Enumerated(EnumType.STRING)
    @Inject
    private PipeTaskAction pipeTaskAction;
    @Column(name = DBNames.PIPE_TASK_EXECUTION_STATUS)
    @Enumerated(EnumType.STRING)
    private ExecutionStatus taskExecutionStatus;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Pipeline getPipeline() {
        return pipeline;
    }

    public void setPipeline(Pipeline pipeline) {
        this.pipeline = pipeline;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDateTime getTaskStartTime() {
        return taskStartTime;
    }

    public void setTaskStartTime(LocalDateTime taskStartTime) {
        this.taskStartTime = taskStartTime;
    }

    public LocalDateTime getTaskEndTime() {
        return taskEndTime;
    }

    public void setTaskEndTime(LocalDateTime taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    public ExecutionStatus getTaskExecutionStatus() {
        return taskExecutionStatus;
    }

    public void setTaskExecutionStatus(ExecutionStatus taskExecutionStatus) {
        this.taskExecutionStatus = taskExecutionStatus;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public PipeTaskAction getPipeTaskAction() {
        return pipeTaskAction;
    }

    public void setPipeTaskAction(PipeTaskAction pipeTaskAction) {
        this.pipeTaskAction = pipeTaskAction;
    }
}
