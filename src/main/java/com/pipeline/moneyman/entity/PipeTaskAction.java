package com.pipeline.moneyman.entity;


import com.pipeline.moneyman.exception.FailedTaskException;
import com.pipeline.moneyman.service.impl.PipeTaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public enum PipeTaskAction {
    PRINT {
        @Override
        public void action(PipeTask pipeTask) {
            LOGGER.info(pipeTask.getTaskName());
            pipeTask.setTaskExecutionStatus(ExecutionStatus.COMPLETED);
        }
    }, RANDOM {
        @Override
        public void action(PipeTask pipeTask) {
            LOGGER.info(pipeTask.getTaskName());
            sleepForSeconds(RANDOM_ACTION_TIME);
            ExecutionStatus randomStatus = ExecutionStatus.getRandomExecutionStatus();
            pipeTask.setTaskExecutionStatus(randomStatus);
            //checkRandomResult(pipeTask);
        }
    }, COMPLETED {
        @Override
        public void action(PipeTask pipeTask) {
            LOGGER.info(pipeTask.getTaskName());
            sleepForSeconds(COMPLETE_ACTION_TIME);
            pipeTask.setTaskExecutionStatus(ExecutionStatus.COMPLETED);
        }
    }, DELAYED {
        @Override
        public void action(PipeTask pipeTask) {
            LOGGER.info(pipeTask.getTaskName());
            sleepForSeconds(DELAYED_ACTION_TIME);
            pipeTask.setTaskExecutionStatus(ExecutionStatus.COMPLETED);
        }
    };
    private final static Logger LOGGER = LogManager.getLogger();
    private final static long COMPLETE_ACTION_TIME = 1L;
    private final static long RANDOM_ACTION_TIME = 1L;
    private final static long DELAYED_ACTION_TIME = 10L;

    public abstract void action(PipeTask pipeTask);

    private static final void sleepForSeconds(long seconds) throws FailedTaskException {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            throw new FailedTaskException();
        }
    }
}
